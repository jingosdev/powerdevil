
#### Description
powerdevil

# powerdevil
powerdevil is based on kde powerdevil

#### Installation

```
mkdir build
cd build
cmake ..
make
sudo make install
```

## Features
* insert dimdisplay dbus interface
* init value update

## Links
- Home: [www.jingos.com](http://www.jingos.com/)
- Project page: https://github.com/zhangalps/powerdevil
- File issues: github.com/jingOS-team/powerdevil/issues
- Development channel: [www.jingos.com](http://www.jingos.com/)


#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request
